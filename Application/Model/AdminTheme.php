<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ThemeModule\Application\Model;

use OxidEsales\Eshop\Core\Exception\StandardException;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\SettingsHandler;
use OxidEsales\Eshop\Core\Theme;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;
use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * Class AdminTheme.
 */
class AdminTheme extends Theme
{
    use DataGetter;

    /**
     *  default oxid admin theme ident.
     */
    protected const OXADMIN_STANDARD_THEME = 'admin';

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws StandardException
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    public function activate()
    {
        $sError = $this->checkForActivationErrors();
        if ($sError) {
            /** @var StandardException $oException */
            $oException = oxNew(StandardException::class, $sError);

            throw $oException;
        }

        $this->saveConfiguration($this->getInfo('parentTheme'));

        /** @var SettingsHandler::class $settingsHandler */
        $settingsHandler = Registry::get(SettingsHandler::class);
        $settingsHandler->setModuleType('theme')->run($this);
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function getActiveThemesList()
    {
        $oConfig = Registry::getConfig();

        $activeThemeList = [];
        if ($this->isAdmin() === false) {
            return $activeThemeList;
        }

        $adminTheme = $oConfig->getConfigParam('sAdminTheme');
        if ($adminTheme) {
            $activeThemeList[] = $adminTheme;
        }

        $adminCustomTheme = $oConfig->getConfigParam('sAdminCustomTheme');
        if ($adminCustomTheme) {
            $activeThemeList[] = $adminCustomTheme;
        }

        if (empty($activeThemeList)) {
            $activeThemeList[] = self::OXADMIN_STANDARD_THEME;
        }

        return $activeThemeList;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function getActiveThemeId()
    {
        $oConfig = Registry::getConfig();

        $adminCustomTheme = $oConfig->getConfigParam('sAdminCustomTheme');
        if ($adminCustomTheme) {
            return $adminCustomTheme;
        }

        $adminTheme = $oConfig->getConfigParam('sAdminTheme');
        if ($adminTheme) {
            return $adminTheme;
        }

        return self::OXADMIN_STANDARD_THEME;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function getList()
    {
        $this->_aThemeList = [];
        $sOutDir = Registry::getConfig()->getViewsDir();
        foreach (glob($sOutDir . '*', GLOB_ONLYDIR) as $sDir) {
            /** @var self $adminTheme */
            $adminTheme = oxNew(self::class);
            $directoryName = basename($sDir);

            if ($adminTheme->load($directoryName)) {
                $this->_aThemeList[$sDir] = $adminTheme;
            }
        }

        return $this->_aThemeList;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function load($adminThemeId)
    {
        $filePath = Registry::getConfig()->getViewsDir() . $adminThemeId . '/adminTheme.php';
        if (file_exists($filePath) && is_readable($filePath)) {
            $themeParameter = [];

            include $filePath;
            $this->_aTheme = $themeParameter;
            $this->_aTheme['id'] = $adminThemeId;
            $this->_aTheme['active'] = ($this->getActiveThemeId() === $adminThemeId);

            return true;
        }

        if ($adminThemeId === self::OXADMIN_STANDARD_THEME) {
            $this->loadOxAdmin();

            return true;
        }

        return false;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function getParent()
    {
        $sParent = $this->getInfo('parentTheme');
        if (!$sParent) {
            return null;
        }
        $oTheme = oxNew(self::class);
        if ($oTheme->load($sParent)) {
            return $oTheme;
        }

        return null;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    public function saveConfiguration($sParent = null, $iShopId = null)
    {
        $iShopId ??= Registry::getConfig()->getShopId();

        $adminTheme = $this->getId();
        $adminCustomTheme = '';

        if ($sParent) {
            $adminCustomTheme = $adminTheme;
            $adminTheme = $sParent;
        }

        ToolsConfig::saveConfigParam('sAdminTheme', $adminTheme, 'str', '', $iShopId);
        ToolsConfig::saveConfigParam('sAdminCustomTheme', $adminCustomTheme, 'str', '', $iShopId);
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function loadOxAdmin()
    {
        $this->_aTheme = [
            'id'          => self::OXADMIN_STANDARD_THEME,
            'active'      => ($this->getActiveThemeId() === self::OXADMIN_STANDARD_THEME),
            'title'       => 'Oxid Standard Admin',
            'description' => 'This is OXID eShop\'s official admin theme.',
            'thumbnail'   => 'theme.png',
            'version'     => '1.0',
            'author'      => '<a href="https://www.oxid-esales.com" title="OXID eSales AG">OXID eSales AG</a>',
            'settings'    => [],
        ];

        return true;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function getThumbnailUrl()
    {
        $oConfig = Registry::getConfig();

        $filePath = $oConfig->getOutDir()
            . $this->getActiveThemeId()
            . DIRECTORY_SEPARATOR
            . $this->getInfo('thumbnail');
        if (file_exists($filePath) && is_readable($filePath)) {
            return $oConfig->getOutUrl()
                . $this->getActiveThemeId()
                . DIRECTORY_SEPARATOR
                . $this->getInfo('thumbnail');
        }

        return null;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function getTemplates()
    {
        $templatePath = Registry::getConfig()->getTemplateDir(true);

        $templates = glob($templatePath . '*.tpl');
        if (is_array($templates)) {
            array_walk(
                $templates,
                static function (&$item) {
                    $item = basename($item);
                }
            );

            return $templates;
        }

        return [];
    }
}
