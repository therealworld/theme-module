<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ThemeModule\Application\Controller\Admin;

use OxidEsales\Eshop\Application\Controller\Admin\ShopConfiguration;
use OxidEsales\Eshop\Core\Config;
use OxidEsales\Eshop\Core\Exception\StandardException;
use OxidEsales\Eshop\Core\Registry;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use TheRealWorld\ThemeModule\Application\Model\AdminTheme;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;

/**
 * Class Settings.
 */
class AdminThemeConfiguration extends ShopConfiguration
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sThisTemplate = 'adminthemeconfig.tpl';

    protected string $currentAdminThemeID = '';

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function render()
    {
        $return = parent::render();
        $adminTheme = $this->getLoadedAdminTheme();
        $this->currentAdminThemeID = $adminTheme->getId();
        $this->addTplParam('currentAdminTheme', $adminTheme);
        $this->addTplParam('oxid', $this->currentAdminThemeID);
        $this->addTplParam('sHelpURL', '');

        $aDbVariables = $this->loadConfVars(Registry::getConfig()->getShopId(), $this->_getModuleForConfigVars());
        $this->addTplParam('var_constraints', $aDbVariables['constraints']);
        $this->addTplParam('var_grouping', $aDbVariables['grouping']);
        foreach ($this->_aConfParams as $sType => $sParam) {
            $this->addTplParam($sParam, $aDbVariables['vars'][$sType]);
        }

        return $return;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    public function saveConfVars()
    {
        $request = Registry::getRequest();
        $this->save();

        $sModule = $this->_getModuleForConfigVars();

        foreach ($this->_aConfParams as $sType => $sParam) {
            $aConfVars = $request->getRequestParameter($sParam);
            if (is_array($aConfVars)) {
                foreach ($aConfVars as $sName => $mValue) {
                    ToolsConfig::saveConfigParam($sName, $mValue, $sType, $sModule);
                }
            }
        }
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected function _getModuleForConfigVars()
    {
        if (empty($this->currentAdminThemeID)) {
            $this->currentAdminThemeID = $this->getEditObjectId();
        }

        return Config::OXMODULE_THEME_PREFIX . $this->currentAdminThemeID;
    }

    protected function getLoadedAdminTheme(): AdminTheme
    {
        /** @var AdminTheme $adminTheme */
        $adminTheme = oxNew(AdminTheme::class);
        $currentAdminThemeId = $this->getEditObjectId();

        if (!$adminTheme->load($currentAdminThemeId)) {
            $message = sprintf(
                '%s: %s',
                Registry::getLang()->translateString('TRWTHEME_ADMINTHEME_NOT_LOADED'),
                $currentAdminThemeId
            );

            Registry::getUtilsView()->addErrorToDisplay(
                oxNew(StandardException::class, $message)
            );
        }

        return $adminTheme;
    }
}
