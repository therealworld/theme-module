<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ThemeModule\Application\Controller\Admin;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Theme;

/**
 * ModuleConfiguration class.
 *
 * @mixin \OxidEsales\Eshop\Application\Controller\Admin\ModuleConfiguration
 */
class ModuleConfiguration extends ModuleConfiguration_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function render()
    {
        $result = parent::render();

        $moduleId = $this->_sModuleId;
        if ($moduleId === 'trwtheme') {
            $oConfig = Registry::getConfig();
            $bTRWThemeOnlyChilds = $oConfig->getConfigParam('bTRWThemeOnlyChilds');

            $oThemes = oxNew(Theme::class);
            $aThemeIds = [];
            foreach ($oThemes->getList() as $oTheme) {
                if ($bTRWThemeOnlyChilds && !$oTheme->getParent()) {
                    continue;
                }
                $aThemeIds[] = $oTheme->getId();
            }
            $this->_aViewData['var_constraints']['sTRWThemeMainTheme'] = $aThemeIds;
        }

        return $result;
    }
}
