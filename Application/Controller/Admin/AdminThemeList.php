<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ThemeModule\Application\Controller\Admin;

use OxidEsales\Eshop\Application\Controller\Admin\AdminListController;
use TheRealWorld\ThemeModule\Application\Model\AdminTheme;

/**
 * Class AdminList.
 */
class AdminThemeList extends AdminListController
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sThisTemplate = 'adminthemelist.tpl';

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function render()
    {
        parent::render();
        $editObjectId = $this->getEditObjectId();

        $this->addTplParam('adminThemeList', $this->getAdminThemeList());
        $this->addTplParam('oxid', $editObjectId);
        if (empty($editObjectId)) {
            $this->setEditObjectId($editObjectId);
            $this->addTplParam('updatelist', 0);
        }
        $this->addTplParam('selectedAdminThemeId', $editObjectId);

        return $this->_sThisTemplate;
    }

    public function getAdminThemeList(): array
    {
        return $this->getAdminTheme()->getList();
    }

    public function getAdminTheme(): AdminTheme
    {
        return oxNew(AdminTheme::class);
    }
}
