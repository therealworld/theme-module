<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ThemeModule\Application\Controller\Admin;

use OxidEsales\Eshop\Application\Controller\Admin\AdminController;
use TheRealWorld\ThemeModule\Application\Model\AdminTheme;

/**
 * Class Base.
 */
class AdminThemeController extends AdminController
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sThisTemplate = 'admintheme.tpl';

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function render()
    {
        $return = parent::render();

        $this->addTplParam('oxid', $this->getAdminTheme()->getActiveThemeId());

        return $return;
    }

    public function getAdminTheme(): AdminTheme
    {
        return oxNew(AdminTheme::class);
    }
}
