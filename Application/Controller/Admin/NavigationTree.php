<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ThemeModule\Application\Controller\Admin;

use OxidEsales\Eshop\Core\Module\ModuleList;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\EshopCommunity\Internal\Framework\Theme\Bridge\AdminThemeBridgeInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use TheRealWorld\ThemeModule\Core\Config;

/**
 * Class NavigationTree.
 */
class NavigationTree extends NavigationTree_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    protected function _getMenuFiles()
    {
        $filesToLoad = [];

        /** @var Config $config */
        $config = Registry::getConfig();
        $viewsPath = $config->getViewsDir();
        $adminTheme = $config->getShopConfVar('sAdminTheme') ??
            $this->getContainer()->
                get(AdminThemeBridgeInterface::class)->
                getActiveTheme();

        $fullAdminDir = $viewsPath . $adminTheme . DIRECTORY_SEPARATOR;
        $menuFile = $fullAdminDir . 'menu.xml';

        // including std file
        if (file_exists($menuFile)) {
            $filesToLoad[] = $menuFile;
        }

        // including custom file
        if (file_exists($fullAdminDir . 'user.xml')) {
            $filesToLoad[] = $fullAdminDir . 'user.xml';
        }

        $adminTheme = $config->getShopConfVar('sAdminCustomTheme');
        $fullAdminDir = $viewsPath . $adminTheme . DIRECTORY_SEPARATOR;
        $menuFile = $fullAdminDir . 'menu.xml';

        // including std file
        if (file_exists($menuFile)) {
            $filesToLoad[] = $menuFile;
        }

        // including custom file
        if (file_exists($fullAdminDir . 'user.xml')) {
            $filesToLoad[] = $fullAdminDir . 'user.xml';
        }

        // including module menu files
        $path = getShopBasePath();
        $moduleList = oxNew(ModuleList::class);
        $activeModuleInfo = $moduleList->getActiveModuleInfo();
        if (is_array($activeModuleInfo)) {
            foreach ($activeModuleInfo as $modulePath) {
                $fullPath = $path . 'modules/' . $modulePath;
                // missing file/folder?
                if (is_dir($fullPath)) {
                    // including menu file
                    $menuFile = $fullPath . '/menu.xml';
                    if (file_exists($menuFile) && is_readable($menuFile)) {
                        $filesToLoad[] = $menuFile;
                    }
                }
            }
        }

        $parentFilesToLoad = parent::_getMenuFiles();

        return array_merge($filesToLoad, $parentFilesToLoad);
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected function _mergeNodes($domElemTo, $domElemFrom, $xPathTo, $domDocTo, $queryStart)
    {
        if (is_object($domElemFrom) && property_exists($domElemFrom, 'childNodes')) {
            parent::_mergeNodes($domElemTo, $domElemFrom, $xPathTo, $domDocTo, $queryStart);
        }
    }
}
