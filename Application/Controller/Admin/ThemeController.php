<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ThemeModule\Application\Controller\Admin;

use OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController;
use OxidEsales\Eshop\Core\Exception\DatabaseException;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Str;
use OxidEsales\Eshop\Core\Theme;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;
use TheRealWorld\ToolsPlugin\Core\ToolsTheme;
use TheRealWorld\ToolsPlugin\Core\ToolsYaml;

/**
 * provides a Child-Theme-Configurator
 * Admin Menu: Extensions -> ChildTheme Configurator.
 */
class ThemeController extends AdminDetailsController
{
    /** theme-Obj of active theme */
    protected ?Theme $_oThemeObj = null;

    /** theme-ID of active theme */
    protected ?string $_sThemeId = null;

    /** theme-ID of parent theme */
    protected ?string $_mParentThemeId = null;

    /** theme-title of active theme */
    protected ?string $_sThemeTitle = null;

    /** theme-title of parent theme */
    protected ?string $_mParentThemeTitle = null;

    /** possible Config Types */
    protected array $_aPossibleConfigTypes = [
        'bool', 'str', 'arr', 'aarr', 'select',
    ];

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sThisTemplate = 'themeconfig.tpl';

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws DatabaseException
     */
    public function render()
    {
        parent::render();

        if ($this->getParentThemeId()) {
            $this->_aViewData['aThemeOptions'] = ToolsTheme::getThemeOptions($this->getThemeId());

            // Type-List for creating new options
            $aPossibleConfigTypes = [];
            foreach ($this->_aPossibleConfigTypes as $sPossibleConfigType) {
                $aPossibleConfigTypes[] = [
                    'id'   => $sPossibleConfigType,
                    'name' => Registry::getLang()->translateString(
                        'TRWTHEME_CONFIG_TYPE_' . Str::getStr()->strtoupper($sPossibleConfigType)
                    ),
                ];
            }
            $this->_aViewData['aPossibleConfigTypes'] = $aPossibleConfigTypes;

            // Group-List for creating new options
            $this->_aViewData['aPossibleOptionGroups'] = ToolsTheme::getOptionGroups($this->getThemeId());

            // some URLs that make sense only if you are in Childtheme
            $this->_aViewData['sTransferOptionUrl'] = $this->getViewConfig()->getSelfLink() .
                'cl=themecontroller&fnc=runTransferThemeOptions';
            $this->_aViewData['sDeleteAllOptionsUrl'] = $this->getViewConfig()->getSelfLink() .
                'cl=themecontroller&fnc=runDeleteAllOptions';
        }

        // set the new size for optionlength
        $aSize = [];
        $aSize[] = (int) ToolsDB::getFieldSize('oxconfig', 'oxvarname');
        $aSize[] = (int) ToolsDB::getFieldSize('oxconfigdisplay', 'oxcfgvarname');
        $iActualOptionLength = min($aSize);
        $iNewSettingSize = ($iActualOptionLength !== 100 ? 100 : 255);
        $this->_aViewData['sSetOptionLengthUrl'] = $this->getViewConfig()->getSelfLink() .
            'cl=themecontroller&fnc=runSetFieldSize&size=' . $iNewSettingSize;
        $this->_aViewData['iSetOptionLength'] = $iNewSettingSize;
        $this->_aViewData['iActualOptionLength'] = $iActualOptionLength;

        // List of all themes
        $this->_oThemeObj = $this->_getThemeObj();
        $this->_aViewData['aThemes'] = $this->_oThemeObj->getList();

        return $this->_sThisTemplate;
    }

    public function runSetFieldSize(): void
    {
        $iSize = Registry::getRequest()->getRequestParameter('size');
        if ($iSize) {
            ToolsDB::setFieldSize('oxconfig', 'oxvarname', $iSize);
            ToolsDB::setFieldSize('oxconfigdisplay', 'oxcfgvarname', $iSize);
            $this->_aViewData['sSuccess'] = Registry::getLang()->translateString('TRWTHEME_SUCCESS_SETFIELDSIZE');
        }
    }

    /** Export the Theme option.
     * @throws DatabaseException
     */
    public function runExportThemeOptions(): void
    {
        $aOptions = Registry::getRequest()->getRequestParameter('editval');

        if ($aOptions['exporttheme'] && $aThemeValues = ToolsTheme::getThemeOptions($aOptions['exporttheme'])) {
            $result = ToolsYaml::getYamlFromArray($aThemeValues);
            header('Content-type: application/x-yaml');
            header('Content-Description: File Transfer');
            header('Content-Disposition: attachment; filename="' . $aOptions['exporttheme'] . '.yaml"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . strlen($result));
            echo $result;
            Registry::getUtils()->showMessageAndExit('');
        }
        $this->_aViewData['sExportError'] = true;
    }

    /** Import the Theme option */
    public function runImportThemeOptions(): void
    {
        $bResult = false;
        $aOptions = Registry::getRequest()->getRequestParameter('editval');

        $sFile = ToolsConfig::uploadFile('fileThemeUpload', '', true);

        if (
            $aOptions['targettheme']
            && ($sYaml = ToolsYaml::getYamlStringFromFile($sFile))
            && $aThemeValues = ToolsYaml::getYamlContent($sYaml)
        ) {
            ToolsTheme::setThemeOptions($aOptions['targettheme'], $aThemeValues);
            $bResult = true;
        }

        if ($bResult) {
            $this->_aViewData['sSuccess'] = Registry::getLang()->translateString('TRWTHEME_SUCCESS_IMPORTTHEMEOPTIONS');
        } else {
            $this->_aViewData['sImportError'] = true;
        }
    }

    /** edit a Option of the theme */
    public function runEditOption(): void
    {
        $aOptions = Registry::getRequest()->getRequestParameter('editval');

        if ($aOptions['editoption']) {
            // show the option
            if (!$aOptions['editoptiontype']) {
                $this->_aViewData['sEditOption'] = $aOptions['editoption'];
                $this->_aViewData['sEditOptionType'] = ToolsDB::getAnyId(
                    'oxconfig',
                    [
                        'oxmodule'  => 'theme:' . $this->getThemeId(),
                        'oxvarname' => $aOptions['editoption'],
                    ],
                    'oxvartype'
                );
                $this->_aViewData['sEditOptionGroup'] = ToolsDB::getAnyId(
                    'oxconfigdisplay',
                    [
                        'oxcfgmodule'  => 'theme:' . $this->getThemeId(),
                        'oxcfgvarname' => $aOptions['editoption'],
                    ],
                    'oxgrouping'
                );
            } else {
                $sOption = $aOptions['editoptionnew'] ?: $aOptions['editoption'];
                $sOptionGroup = $aOptions['editoptiongroupnew'] ?: $aOptions['editoptiongroup'];

                // delete the old option before save
                ToolsTheme::deleteThemeOptions($this->getThemeId(), [$sOption]);

                // save the selectoption
                $aThemeValue = [$sOption => [
                    'group'       => $sOptionGroup,
                    'type'        => $aOptions['editoptiontype'],
                    'value'       => '',
                    'constraints' => '',
                    'pos'         => '',
                ]];

                ToolsTheme::setThemeOptions($this->getThemeId(), $aThemeValue);

                // check if we need translations for group
                if ($aOptions['editoptiongroupnew']) {
                    $this->_aViewData['sNewOptionGroupNote'] = $sOptionGroup;
                }
                if ($aOptions['editoptionnew']) {
                    $this->_aViewData['sNewOptionNameNote'] = $sOption;
                }
                $this->_aViewData['sSuccess'] = Registry::getLang()->translateString(
                    'TRWTHEME_SUCCESS_SETNEWTHEMEOPTION'
                );
            }
        }
    }

    /**
     * edit a Select Option of the theme.
     *
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    public function runEditSelectOption(): void
    {
        $aOptions = Registry::getRequest()->getRequestParameter('editval');

        if ($aOptions['editselectoption']) {
            $sCfgVarname = $aOptions['editselectoption'];
            $sCfgModule = 'theme:' . $this->getThemeId();

            // show the selectoption
            if (!$aOptions['editselectvalue']) {
                $this->_aViewData['sEditSelectOption'] = $aOptions['editselectoption'];
                $this->_aViewData['sEditSelectValue'] = ToolsConfig::getShopConfigConstraint($sCfgVarname, $sCfgModule);
            } else {
                // save the selectoption
                ToolsConfig::setShopConfigConstraint($sCfgVarname, $sCfgModule, $aOptions['editselectvalue']);
            }
        }
    }

    /** rename a OptionGroup */
    public function runRenameGroup(): void
    {
        $aOptions = Registry::getRequest()->getRequestParameter('editval');

        if (
            $aOptions['renamegroup']
            && $aOptions['renamegroupnew']
            && ToolsConfig::renameShopConfigThemeGroup(
                $aOptions['renamegroup'],
                $aOptions['renamegroupnew'],
                $this->getThemeId()
            )
        ) {
            $this->_aViewData['sNewOptionGroupNote'] = $aOptions['renamegroupnew'];
        }
    }

    /**
     * Set New Theme option.
     */
    public function runSetNewThemeOption(): void
    {
        $aOptions = Registry::getRequest()->getRequestParameter('editval');
        if (
            (
                $aOptions['themenewoptiongroup']
                || $aOptions['themenewoptiongroupnew']
            )
            && $aOptions['themenewoptionname']
            && $aOptions['themenewoptiontype']
            && in_array($aOptions['themenewoptiontype'], $this->_aPossibleConfigTypes, true)
        ) {
            $sOptionGroup = $aOptions['themenewoptiongroup'] ?: $aOptions['themenewoptiongroupnew'];
            $aThemeValue = [$aOptions['themenewoptionname'] => [
                'group'       => $sOptionGroup,
                'type'        => $aOptions['themenewoptiontype'],
                'value'       => '',
                'constraints' => '',
                'pos'         => '',
            ]];

            ToolsTheme::setThemeOptions($this->getThemeId(), $aThemeValue);

            // check if we need translations for group
            if ($aOptions['themenewoptiongroupnew']) {
                $this->_aViewData['sNewOptionGroupNote'] = $sOptionGroup;
            }
            $this->_aViewData['sNewOptionNameNote'] = $aOptions['themenewoptionname'];
            $this->_aViewData['sSuccess'] = Registry::getLang()->translateString('TRWTHEME_SUCCESS_SETNEWTHEMEOPTION');
        }
    }

    /**
     * transfer Theme options to child-Theme.
     *
     * @throws DatabaseException
     */
    public function runTransferThemeOptions(): void
    {
        if ($this->getParentThemeId()) {
            $aThemeValues = ToolsTheme::getThemeOptions($this->getParentThemeId());
            ToolsTheme::setThemeOptions($this->getThemeId(), $aThemeValues);
            $this->_aViewData['sSuccess'] = Registry::getLang()->translateString(
                'TRWTHEME_SUCCESS_TRANSFERTHEMEOPTIONS'
            );
        }
    }

    /** delete all Theme options */
    public function runDeleteAllThemeOption(): void
    {
        $aOptions = Registry::getRequest()->getRequestParameter('editval');
        if ($aOptions['deleteallthemeoption']) {
            ToolsTheme::deleteThemeOptions($aOptions['deleteallthemeoption']);
            $this->_aViewData['sSuccess'] = Registry::getLang()->translateString(
                'TRWTHEME_SUCCESS_DELETEALLTHEMEOPTION'
            );
        }
    }

    /** Delete a Theme option */
    public function runDeleteThemeOption(): void
    {
        $aOptions = Registry::getRequest()->getRequestParameter('editval');
        if ($aOptions['deletethemeoption']) {
            ToolsTheme::deleteThemeOptions($this->getThemeId(), [$aOptions['deletethemeoption']]);
            $this->_aViewData['sSuccess'] = Registry::getLang()->translateString('TRWTHEME_SUCCESS_DELETETHEMEOPTION');
        }
    }

    /** Get the Parent Theme Id (Template Getter) */
    public function getParentThemeId(): ?string
    {
        if (is_null($this->_mParentThemeId)) {
            $this->_oThemeObj = $this->_getThemeObj();
            if ($sParentThemeId = $this->_oThemeObj->getInfo('parentTheme')) {
                $this->_mParentThemeId = $sParentThemeId;
            }
        }

        return $this->_mParentThemeId;
    }

    /** Get the Parent Theme Title (Template Getter) */
    public function getParentThemeTitle(): string
    {
        if (is_null($this->_mParentThemeTitle)) {
            $this->_mParentThemeTitle = '';

            if ($this->getParentThemeId()) {
                $oTheme = oxNew(Theme::class);
                $oTheme->load($this->getParentThemeId());
                $this->_mParentThemeTitle = $oTheme->getInfo('title');
            }
        }

        return $this->_mParentThemeTitle;
    }

    /** Get the active Theme Id (Template Getter) */
    public function getThemeId(): string
    {
        if (is_null($this->_sThemeId)) {
            $this->_oThemeObj = $this->_getThemeObj();
        }

        return $this->_sThemeId;
    }

    /** Get the active Theme Title (Template Getter) */
    public function getThemeTitle(): string
    {
        if (is_null($this->_sThemeTitle) && $this->_getThemeObj()) {
            $this->_sThemeTitle = $this->_oThemeObj->getInfo('title');
        }

        return $this->_sThemeTitle;
    }

    /** Get the active Theme Object */
    protected function _getThemeObj(): Theme
    {
        if (is_null($this->_oThemeObj)) {
            $this->_oThemeObj = oxNew(Theme::class);
            $this->_sThemeId = $this->_oThemeObj->getActiveThemeId();
            $this->_oThemeObj->load($this->_sThemeId);
        }

        return $this->_oThemeObj;
    }
}
