<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ThemeModule\Application\Controller\Admin;

use OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController;
use OxidEsales\Eshop\Core\Exception\StandardException;
use OxidEsales\Eshop\Core\Module\ModuleVariablesLocator;
use OxidEsales\Eshop\Core\Registry;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use TheRealWorld\ThemeModule\Application\Model\AdminTheme;

/**
 * Class Main.
 */
class AdminThemeMain extends AdminDetailsController
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected $_sThisTemplate = 'adminthememain.tpl';

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function render()
    {
        $return = parent::render();
        $adminTheme = $this->getLoadedAdminTheme();
        $adminThemeId = $adminTheme->getId();
        $this->addTplParam('currentAdminTheme', $adminTheme);
        $this->addTplParam('oxid', $adminThemeId);

        $this->addTplParam('sHelpURL', '');

        return $return;
    }

    /**
     * @throws StandardException
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    public function setAdminTheme(): void
    {
        $this->getLoadedAdminTheme()->activate();
        $this->resetCache();
    }

    /**
     * Resets template, language and menu xml cache.
     */
    public function resetCache(): void
    {
        $templates = $this->getLoadedAdminTheme()->getTemplates();

        $utils = Registry::getUtils();
        $utils->resetTemplateCache($templates);
        $utils->resetLanguageCache();
        $utils->resetMenuCache();

        ModuleVariablesLocator::resetModuleVariables();
    }

    protected function getLoadedAdminTheme(): AdminTheme
    {
        /** @var AdminTheme $adminTheme */
        $adminTheme = oxNew(AdminTheme::class);
        $currentAdminThemeId = $this->getEditObjectId();

        if ($adminTheme->load($currentAdminThemeId) === false) {
            $message = sprintf(
                '%s: %s',
                Registry::getLang()->translateString('TRWTHEME_ADMINTHEME_NOT_LOADED'),
                $currentAdminThemeId
            );

            Registry::getUtilsView()->addErrorToDisplay(
                oxNew(StandardException::class, $message)
            );
        }

        return $adminTheme;
    }
}
