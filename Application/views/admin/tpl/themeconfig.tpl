[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign}]
[{assign var="sParentThemeId" value=$oView->getParentThemeId()}]

<form name="transfer" id="transfer" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="oxid" value="1" />
    <input type="hidden" name="cl" value="" />
</form>
<h1>[{oxmultilang ident="TRWTHEME_OPTIONS"}]</h1>
[{if $sSuccess}]
    <p class="messagebox">[{$sSuccess}]</p>
[{/if}]
[{if $sNewOptionGroupNote}]
    <p class="errorbox">[{oxmultilang ident="TRWTHEME_OPTION_GROUP_NOTE"}]: [{$sNewOptionGroupNote}]</p>
[{/if}]
[{if $sNewOptionNameNote}]
    <p class="errorbox">[{oxmultilang ident="TRWTHEME_OPTION_NAME_NOTE"}]: [{$sNewOptionNameNote}]</p>
[{/if}]
[{if $sImportError}]
    <p class="errorbox">[{oxmultilang ident="TRWTHEME_IMPORT_ERROR"}]</p>
[{/if}]
[{if $sExportError}]
    <p class="errorbox">[{oxmultilang ident="TRWTHEME_EXPORT_ERROR"}]</p>
[{/if}]
<h3>[{oxmultilang ident="GENERAL_REVIEW"}]</h3>
<table cellspacing="0" cellpadding="0" border="0">
    <colgroup>
        <col width="210" nowrap />
        <col />
    </colgroup>
    <tr>
        <td class="edittext">
            [{oxmultilang ident="TRWTHEME_ACTIVETHEME"}]:
        </td>
        <td class="edittext">
            [{$oView->getThemeTitle()}] (ID: [{$oView->getThemeId()}])
        </td>
    </tr>
    <tr>
        <td class="edittext">
            [{oxmultilang ident="TRWTHEME_PARENTTHEME"}]:
        </td>
        <td class="edittext">
            [{if $sParentThemeId}]
                [{$oView->getParentThemeTitle()}] (ID: [{$sParentThemeId}])
            [{else}]
                [{oxmultilang ident="TRWTHEME_CHILDTHEME_WARNING"}]
            [{/if}]
        </td>
    </tr>
    [{if $sParentThemeId}]
        <tr>
            <td class="edittext">
                [{oxmultilang ident="TRWTHEME_TRANSFER"}]
            </td>
            <td class="edittext">
                <a href="[{$sTransferOptionUrl}]">[{oxmultilang ident="TRWTHEME_TRANSFER_GO"}]</a> [{oxinputhelp ident="TRWTHEME_TRANSFER_HELP"}]
            </td>
        </tr>
    [{/if}]
    <tr>
        <td class="edittext">
            [{oxmultilang ident="TRWTHEME_SETOPTIONLENGTH"}]
        </td>
        <td class="edittext">
            [{oxmultilang ident="TRWTHEME_ACTUAL"}]: [{$iActualOptionLength}]  [{oxmultilang ident="TRWTHEME_NEW"}]: <a href="[{$sSetOptionLengthUrl}]">[{$iSetOptionLength}]</a> [{oxinputhelp ident="TRWTHEME_SETOPTIONLENGTH_HELP"}]
        </td>
    </tr>

</table>
[{if $sParentThemeId}]
<h3>[{oxmultilang ident="TRWTHEME_OPTION_EDIT"}]</h3>
<form name="myedit" id="myedit" action="[{$oViewConf->getSelfLink()}]" method="post">
    <input type="hidden" name="cl" value="themecontroller" />
    <input type="hidden" name="fnc" value="runEditOption" />
    [{$oViewConf->getHiddenSid()}]
    <table cellspacing="0" cellpadding="0" border="0">
        <colgroup>
            <col width="210" nowrap />
            <col />
        </colgroup>
        [{if !$sEditOption}]
            <tr>
                <td class="edittext" valign="top">
                    [{oxmultilang ident="TRWTHEME_OPTION_EDIT"}]
                </td>
                <td class="edittext">
                    <input type="hidden" name="editval[editoption]" value="" />
                    <select name="editval[editoption]" class="editinput">
                        <option value="">[{oxmultilang ident="SHOP_SYSTEM_PLEASE_CHOOSE"}]</option>
                        [{foreach from=$aThemeOptions item=aThemeOption key=sName}]
                        [{assign var="sGroupName" value="SHOP_THEME_GROUP_"|cat:$aThemeOption.group|oxmultilangassign}]
                        [{assign var="sOptionName" value="SHOP_THEME_"|cat:$sName|oxmultilangassign|truncate:50:"...":true}]
                        <option value="[{$sName}]">[{$sGroupName}] - [{$sOptionName}] ([{$sName}])</option>
                        [{/foreach}]
                    </select>
                    [{oxinputhelp ident="TRWTHEME_OPTION_EDIT_HELP"}]
                </td>
            </tr>
        [{else}]
            [{assign var="sEditOptionName" value="SHOP_THEME_"|cat:$sEditOption|oxmultilangassign}]
            <tr>
                <td></td>
                <td class="edittext" valign="top">
                    [{$sEditOptionName}]
                    <input type="hidden" name="editval[editoption]" value="[{$sEditOption}]" />
                </td>
            </tr>
            <tr>
                <td class="edittext" valign="top">
                    [{oxmultilang ident="TRWTHEME_OPTION_GROUP"}]
                </td>
                <td class="edittext">
                    <select name="editval[editoptiongroup]" class="editinput">
                        <option value="">[{oxmultilang ident="SHOP_SYSTEM_PLEASE_CHOOSE"}]</option>
                        [{foreach from=$aPossibleOptionGroups item=aPossibleOptionGroup}]
                        [{assign var="sGroupName" value="SHOP_THEME_GROUP_"|cat:$aPossibleOptionGroup.oxgrouping|oxmultilangassign}]
                        <option value="[{$aPossibleOptionGroup.oxgrouping}]"[{if $aPossibleOptionGroup.oxgrouping == $sEditOptionGroup}] selected[{/if}]>[{$sGroupName}]</option>
                        [{/foreach}]
                    </select>
                    [{oxmultilang ident="GENERAL_OR"}] [{oxmultilang ident="TRWTHEME_GROUP_ADD"}] <input type="text" class="edittext" name="editval[editoptiongroupnew]" value="">
                    [{oxinputhelp ident="TRWTHEME_OPTION_GROUP_HELP"}]
                </td>
            </tr>
            <tr>
                <td class="edittext" valign="top">
                    [{oxmultilang ident="TRWTHEME_OPTION_NAME"}]
                </td>
                <td class="edittext">
                    <input type="text" class="edittext" name="editval[editoptionnew]" value="[{$sEditOption}]" />
                    [{oxinputhelp ident="TRWTHEME_OPTION_NAME_HELP"}]
                </td>
            </tr>
            <tr>
                <td class="edittext" valign="top">
                    [{oxmultilang ident="TRWTHEME_OPTION_TYPE"}]
                </td>
                <td class="edittext">
                    <select name="editval[editoptiontype]" class="editinput">
                        <option value="">[{oxmultilang ident="SHOP_SYSTEM_PLEASE_CHOOSE"}]</option>
                        [{foreach from=$aPossibleConfigTypes item=aPossibleConfigType}]
                        <option value="[{$aPossibleConfigType.id}]"[{if $aPossibleConfigType.id == $sEditOptionType}] selected[{/if}]>[{$aPossibleConfigType.name}]</option>
                        [{/foreach}]
                    </select>
                    [{oxinputhelp ident="TRWTHEME_OPTION_TYPE_HELP"}]
                </td>
            </tr>
        [{/if}]
        <tr>
            <td></td>
            <td class="edittext">
                <input type="submit" class="edittext" name="save" value="[{if $sEditOptionName}][{oxmultilang ident="GENERAL_SAVE"}][{else}][{oxmultilang ident="TRWTHEME_EDIT"}][{/if}]" />
            </td>
        </tr>
    </table>
</form>

<h3>[{oxmultilang ident="TRWTHEME_OPTION_SELECT_EDIT"}]</h3>
<form name="myedit" id="myedit" action="[{$oViewConf->getSelfLink()}]" method="post">
    <input type="hidden" name="cl" value="themecontroller" />
    <input type="hidden" name="fnc" value="runEditSelectOption" />
    [{$oViewConf->getHiddenSid()}]
    <table cellspacing="0" cellpadding="0" border="0">
        <colgroup>
            <col width="210" nowrap />
            <col />
        </colgroup>
        [{if !$sEditSelectOption}]
            <tr>
                <td class="edittext" valign="top">
                    [{oxmultilang ident="TRWTHEME_OPTION_SELECT_EDIT"}]
                </td>
                <td class="edittext">
                    <input type="hidden" name="editval[editselectoption]" value="" />
                    <select name="editval[editselectoption]" class="editinput">
                        <option value="">[{oxmultilang ident="SHOP_SYSTEM_PLEASE_CHOOSE"}]</option>
                        [{foreach from=$aThemeOptions item=aThemeOption key=sName}]
                        [{if $aThemeOption.type == "select"}]
                        [{assign var="sGroupName" value="SHOP_THEME_GROUP_"|cat:$aThemeOption.group|oxmultilangassign}]
                        [{assign var="sOptionName" value="SHOP_THEME_"|cat:$sName|oxmultilangassign|truncate:50:"...":true}]
                        <option value="[{$sName}]">[{$sGroupName}] - [{$sOptionName}] ([{$sName}])</option>
                        [{/if}]
                        [{/foreach}]
                    </select>
                    [{oxinputhelp ident="TRWTHEME_OPTION_SELECT_EDIT_HELP"}]
                </td>
            </tr>
        [{else}]
            [{assign var="sEditSelectOptionName" value="SHOP_THEME_"|cat:$sEditSelectOption|oxmultilangassign}]
            <tr>
                <td>[{$sEditSelectOptionName}]</td>
                <td class="edittext">
                    <input type="hidden" name="editval[editselectoption]" value="[{$sEditSelectOption}]" />
                    <input type="text" name="editval[editselectvalue]" value="[{$sEditSelectValue}]" class="edittext" />
                    [{oxinputhelp ident="TRWTHEME_EDIT_SELECT_VALUE_HELP"}]
                </td>
            </tr>
        [{/if}]
        <tr>
            <td></td>
            <td class="edittext">
                <input type="submit" class="edittext" name="save" value="[{if $sEditSelectOptionName}][{oxmultilang ident="GENERAL_SAVE"}][{else}][{oxmultilang ident="TRWTHEME_EDIT"}][{/if}]" />
            </td>
        </tr>
    </table>
</form>

<h3>[{oxmultilang ident="TRWTHEME_GROUP_RENAME"}]</h3>
<form name="myedit" id="myedit" action="[{$oViewConf->getSelfLink()}]" method="post">
    <input type="hidden" name="cl" value="themecontroller" />
    <input type="hidden" name="fnc" value="runRenameGroup" />
    [{$oViewConf->getHiddenSid()}]
    <table cellspacing="0" cellpadding="0" border="0">
        <colgroup>
            <col width="210" nowrap />
            <col />
        </colgroup>
        <tr>
            <td class="edittext" valign="top">
                [{oxmultilang ident="TRWTHEME_GROUP_RENAME"}]
            </td>
            <td class="edittext">
                <select name="editval[renamegroup]" class="editinput">
                    <option value="">[{oxmultilang ident="SHOP_SYSTEM_PLEASE_CHOOSE"}]</option>
                    [{foreach from=$aPossibleOptionGroups item=aPossibleOptionGroup}]
                    [{assign var="sGroupName" value="SHOP_THEME_GROUP_"|cat:$aPossibleOptionGroup.oxgrouping|oxmultilangassign}]
                    <option value="[{$aPossibleOptionGroup.oxgrouping}]">[{$sGroupName}]</option>
                    [{/foreach}]
                </select>
            </td>
        </tr>
        <tr>
            <td class="edittext" valign="top">
                [{oxmultilang ident="TRWTHEME_GROUP_NEW"}]
            </td>
            <td class="edittext">
                <input type="text" class="edittext" name="editval[renamegroupnew]" value="" />
                [{oxinputhelp ident="TRWTHEME_GROUP_RENAME_HELP"}]
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="edittext">
                <input type="submit" class="edittext" name="save" value="[{oxmultilang ident="GENERAL_SAVE"}]" />
            </td>
        </tr>
    </table>
</form>

<h3>[{oxmultilang ident="TRWTHEME_OPTION_DELETE"}]</h3>
<form name="myedit" id="myedit" action="[{$oViewConf->getSelfLink()}]" method="post">
    <input type="hidden" name="cl" value="themecontroller" />
    <input type="hidden" name="fnc" value="runDeleteThemeOption" />
    [{$oViewConf->getHiddenSid()}]
    <table cellspacing="0" cellpadding="0" border="0">
        <colgroup>
            <col width="210" nowrap />
            <col />
        </colgroup>
        <tr>
            <td class="edittext" valign="top">
                [{oxmultilang ident="TRWTHEME_OPTION_DELETE_ONCE"}]
            </td>
            <td class="edittext">
                <input type="hidden" name="editval[deletethemeoption]" value="" />
                <select name="editval[deletethemeoption]" class="editinput">
                    <option value="">[{oxmultilang ident="SHOP_SYSTEM_PLEASE_CHOOSE"}]</option>
                    [{foreach from=$aThemeOptions item=aThemeOption key=sName}]
                        [{assign var="sGroupName" value="SHOP_THEME_GROUP_"|cat:$aThemeOption.group|oxmultilangassign}]
                        [{assign var="sOptionName" value="SHOP_THEME_"|cat:$sName|oxmultilangassign|truncate:50:"...":true}]
                        <option value="[{$sName}]">[{$sGroupName}] - [{$sOptionName}] ([{$sName}])</option>
                    [{/foreach}]
                </select>
                [{oxinputhelp ident="TRWTHEME_OPTION_DELETE_ONCE_HELP"}]
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="edittext">
                <input type="submit" class="edittext" name="save" value="[{oxmultilang ident="GENERAL_DELETE"}]" />
            </td>
        </tr>
    </table>
</form>
<form name="myedit" id="myedit" action="[{$oViewConf->getSelfLink()}]" method="post">
    <input type="hidden" name="cl" value="themecontroller" />
    <input type="hidden" name="fnc" value="runDeleteAllThemeOption" />
    [{$oViewConf->getHiddenSid()}]
    <table cellspacing="0" cellpadding="0" border="0">
        <colgroup>
            <col width="210" nowrap />
            <col />
        </colgroup>
        <tr>
            <td class="edittext" valign="top">
                [{oxmultilang ident="TRWTHEME_OPTION_DELETE_ALL_FROM_THEME"}]
            </td>
            <td class="edittext">
                <input type="hidden" name="editval[deleteallthemeoption]" value="" />
                <select name="editval[deleteallthemeoption]" class="editinput">
                    <option value="">[{oxmultilang ident="SHOP_SYSTEM_PLEASE_CHOOSE"}]</option>
                    [{foreach from=$aThemes item=oTheme}]
                        <option value="[{$oTheme->getId()}]">[{$oTheme->getId()}]</option>
                    [{/foreach}]
                </select>
                [{oxinputhelp ident="TRWTHEME_OPTION_DELETE_ALL_FROM_THEME_HELP"}]
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="edittext">
                <input type="submit" class="edittext" name="save" value="[{oxmultilang ident="GENERAL_DELETE"}]" />
                <br>
                <a href="[{$sDeleteAllOptionsUrl}]">[{oxmultilang ident="TRWTHEME_OPTION_DELETE_ALL"}]</a>
                [{oxinputhelp ident="TRWTHEME_OPTION_DELETE_ALL_HELP"}]
            </td>
        </tr>
    </table>
</form>
<h3>[{oxmultilang ident="TRWTHEME_SETNEWTHEMEOPTION"}]</h3>
<form name="myedit" id="myedit" action="[{$oViewConf->getSelfLink()}]" method="post">
    <input type="hidden" name="cl" value="themecontroller" />
    <input type="hidden" name="fnc" value="runSetNewThemeOption" />
    [{$oViewConf->getHiddenSid()}]
    <table cellspacing="0" cellpadding="0" border="0">
        <colgroup>
            <col width="210" nowrap />
            <col />
        </colgroup>
        <tr>
            <td class="edittext" valign="top">
                [{oxmultilang ident="TRWTHEME_OPTION_GROUP"}]
            </td>
            <td class="edittext">
                <select name="editval[themenewoptiongroup]" class="editinput">
                    <option value="">[{oxmultilang ident="SHOP_SYSTEM_PLEASE_CHOOSE"}]</option>
                    [{foreach from=$aPossibleOptionGroups item=aPossibleOptionGroup}]
                        [{assign var="sGroupName" value="SHOP_THEME_GROUP_"|cat:$aPossibleOptionGroup.oxgrouping|oxmultilangassign}]
                        <option value="[{$aPossibleOptionGroup.oxgrouping}]">[{$sGroupName}]</option>
                    [{/foreach}]
                </select>
                [{oxmultilang ident="GENERAL_OR"}] [{oxmultilang ident="TRWTHEME_GROUP_ADD"}]
                <input type="text" class="edittext" name="editval[themenewoptiongroupnew]" value="" />
                [{oxinputhelp ident="TRWTHEME_OPTION_GROUP_HELP"}]
            </td>
        </tr>
        <tr>
            <td class="edittext" valign="top">
                [{oxmultilang ident="TRWTHEME_OPTION_NAME"}]
            </td>
            <td class="edittext">
                <input type="text" class="edittext" name="editval[themenewoptionname]" value="" />
                [{oxinputhelp ident="TRWTHEME_OPTION_NAME_HELP"}]
            </td>
        </tr>
        <tr>
            <td class="edittext" valign="top">
                [{oxmultilang ident="TRWTHEME_OPTION_TYPE"}]
            </td>
            <td class="edittext">
                <select name="editval[themenewoptiontype]" class="editinput">
                    <option value="">[{oxmultilang ident="SHOP_SYSTEM_PLEASE_CHOOSE"}]</option>
                    [{foreach from=$aPossibleConfigTypes item=aPossibleConfigType}]
                        <option value="[{$aPossibleConfigType.id}]">[{$aPossibleConfigType.name}]</option>
                    [{/foreach}]
                </select>
                [{oxinputhelp ident="TRWTHEME_OPTION_TYPE_HELP"}]
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="edittext">
                <input type="submit" class="edittext" name="save" value="[{oxmultilang ident="GENERAL_SAVE"}]" />
            </td>
        </tr>
    </table>
</form>
[{/if}]

<h3>[{oxmultilang ident="TRWTHEME_EXPORT"}]</h3>
<form name="myedit" id="myedit" action="[{$oViewConf->getSelfLink()}]" method="post">
    <input type="hidden" name="cl" value="themecontroller" />
    <input type="hidden" name="fnc" value="runExportThemeOptions" />
    [{$oViewConf->getHiddenSid()}]
    <table cellspacing="0" cellpadding="0" border="0">
        <colgroup>
            <col width="210" nowrap />
            <col />
        </colgroup>
        <tr>
            <td class="edittext" valign="top">
                [{oxmultilang ident="TRWTHEME_EXPORT"}]
            </td>
            <td class="edittext">
                <input type="hidden" name="editval[exporttheme]" value="" />
                <select name="editval[exporttheme]" class="editinput">
                    <option value="">[{oxmultilang ident="SHOP_SYSTEM_PLEASE_CHOOSE"}]</option>
                    [{foreach from=$aThemes item=oTheme}]
                        <option value="[{$oTheme->getId()}]"[{if $oTheme->getId() == $sExportTheme}] selected[{/if}]>[{$oTheme->getId()}]</option>
                    [{/foreach}]
                </select>
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="edittext">
                <input type="submit" class="edittext" name="save" value="[{oxmultilang ident="TRWTHEME_EXPORT"}]">
            </td>
        </tr>
    </table>
</form>
<h3>[{oxmultilang ident="TRWTHEME_IMPORT"}]</h3>
<form name="myedit" id="myedit" action="[{$oViewConf->getSelfLink()}]" method="post" enctype="multipart/form-data">
    <input type="hidden" name="cl" value="themecontroller" />
    <input type="hidden" name="fnc" value="runImportThemeOptions" />
    <input type="hidden" name="MAX_FILE_SIZE" value="[{$iMaxUploadFileSize}]" />
    [{$oViewConf->getHiddenSid()}]
    <table cellspacing="0" cellpadding="0" border="0">
        <colgroup>
            <col width="210" nowrap />
            <col />
        </colgroup>
        <tr>
            <td class="edittext" valign="top">
                [{oxmultilang ident="TRWTHEME_IMPORT"}]
            </td>
            <td class="edittext">
                <input type="file" name="fileThemeUpload" /> [{oxinputhelp ident="TRWTHEME_IMPORT_HELP"}]
            </td>
        </tr>
        <tr>
            <td class="edittext">
                [{oxmultilang ident="TRWTHEME_TARGETTHEME"}]
            </td>
            <td class="edittext">
                <input type="text" class="edittext" name="editval[targettheme]" value="[{$sTargetTheme}]" />
                [{oxinputhelp ident="TRWTHEME_TARGETTHEME_HELP"}]
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="edittext">
                <input type="submit" class="edittext" name="save" value="[{oxmultilang ident="GENERAL_SAVE"}]" />
            </td>
        </tr>
    </table>
</form>

[{include file="bottomitem.tpl"}]