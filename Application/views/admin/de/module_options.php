<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwthemeswitcher' => 'Entwickleroptionen',

    'SHOP_MODULE_bTRWThemeSwitcherFrontend' => 'Theme-Wechsler im Frontend aktivieren?',
    'SHOP_MODULE_bTRWThemeOnlyChilds'       => 'Nur Child-Themes im Frontend zeigen',
    'SHOP_MODULE_sTRWThemeMainTheme'        => 'Haupt-Theme',
    'SHOP_MODULE_iTRWThemeResetToMainTheme' => 'Nach wieviel Sekunden Untätigkeit, soll wieder zum Haupt-Theme zurück gewechselt werden?',
];
