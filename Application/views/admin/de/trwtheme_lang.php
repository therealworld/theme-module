<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'mxtrwtheme'                   => 'ChildTheme Konfigurator',
    'mxtrwthemeadmin'              => 'Admin-Themes',
    'mxtrwthemeadminmain'          => 'Stamm',
    'mxtrwthemeadminconfiguration' => 'Einstellungen',

    'TRWTHEME_OPTIONS'                      => 'ChildTheme Konfigurator',
    'TRWTHEME_PARENTTHEME'                  => 'Parenttheme',
    'TRWTHEME_ACTIVETHEME'                  => 'aktives Theme',
    'TRWTHEME_CHILDTHEME_WARNING'           => 'Achtung! Das aktuelle Theme ist kein Childtheme.',
    'TRWTHEME_TRANSFER'                     => 'Parenttheme-Einstellungen',
    'TRWTHEME_TRANSFER_GO'                  => 'übernehmen',
    'TRWTHEME_TRANSFER_HELP'                => 'Transferiert alle noch nicht im Child-Theme vorhandenen Parentheme-Einstellungen in das aktuelle Childtheme.',
    'TRWTHEME_EDIT'                         => 'bearbeiten',
    'TRWTHEME_GROUP_RENAME'                 => 'Umbennenen einer Theme-Option-Gruppen-ID',
    'TRWTHEME_GROUP_RENAME_HELP'            => 'Gib eine der folgenden Theme-Gruppen-IDs eine neue ID.',
    'TRWTHEME_GROUP_NEW'                    => 'Neue Theme-Option-Gruppen-ID',
    'TRWTHEME_GROUP_ADD'                    => 'Gruppe hinzufügen',
    'TRWTHEME_OPTION_EDIT'                  => 'Bearbeite Theme-Option',
    'TRWTHEME_OPTION_SELECT_EDIT'           => 'Bearbeite Theme-Option vom Typ "Select"',
    'TRWTHEME_OPTION_SELECT_EDIT_HELP'      => 'Select-Einstellungen lassen sich nicht in den Theme-Optionen ergänzen. Darum gibt es hier die Möglichkeit die Werte zu manipulieren. Werte = "|" getrennt.',
    'TRWTHEME_OPTION_DELETE'                => 'Lösche Theme-Option(en)',
    'TRWTHEME_OPTION_DELETE_ALL'            => 'Lösche alle Optionen aller Themes',
    'TRWTHEME_OPTION_DELETE_ALL_HELP'       => '!Sollte nur bei großen Aufräumaktionen verwendet werden!',
    'TRWTHEME_OPTION_DELETE_ALL_FROM_THEME' => 'Lösche alle Optionen des Themes',
    'TRWTHEME_OPTION_DELETE_ONCE'           => 'Lösche diese Theme Option',
    'TRWTHEME_OPTION_GROUP'                 => 'Gruppe',
    'TRWTHEME_OPTION_GROUP_NOTE'            => '!Achtung! Ggf. müssen noch Admin-Sprachkonstanten für die Gruppe angelegt oder korrigiert werden.',
    'TRWTHEME_OPTION_NAME'                  => 'Name',
    'TRWTHEME_OPTION_NAME_NOTE'             => '!Achtung! Ggf. müssen noch Admin-Sprachkonstanten für die Variable angelegt oder korrigiert werden.',
    'TRWTHEME_OPTION_TYPE'                  => 'Typ',
    'TRWTHEME_EXPORT'                       => 'Export Theme',
    'TRWTHEME_TARGETTHEME'                  => 'Name des Ziel-Themes',
    'TRWTHEME_TARGETTHEME_HELP'             => 'Geben Sie hier den Namen des Themes an, für das die Theme-Einstellungen importiert werden sollen.',
    'TRWTHEME_IMPORT'                       => 'Import Theme',
    'TRWTHEME_IMPORT_ERROR'                 => 'Achtung! Entweder config-Yaml-File nicht valide oder das Ziel-Theme existiert nicht.',
    'TRWTHEME_EXPORT_ERROR'                 => 'Achtung! Möglicherweise gibt es keine Theme-Einstellungen in Ihrem Export-Theme.',
    'TRWTHEME_SETNEWTHEMEOPTION'            => 'Erstelle neue Childtheme-Option',
    'TRWTHEME_CONFIG_TYPE_BOOL'             => 'Boolean',
    'TRWTHEME_CONFIG_TYPE_STR'              => 'String',
    'TRWTHEME_CONFIG_TYPE_ARR'              => 'Array',
    'TRWTHEME_CONFIG_TYPE_AARR'             => 'Assoziatives Array',
    'TRWTHEME_CONFIG_TYPE_SELECT'           => 'Select-Liste',
    'TRWTHEME_EDIT_SELECT_VALUE_HELP'       => 'Optionen pipe-getrennt notieren. z.B. contact|list|detail',
    'TRWTHEME_SETOPTIONLENGTH'              => 'OXID Einstellungslänge',
    'TRWTHEME_NEW'                          => 'Neu',
    'TRWTHEME_ACTUAL'                       => 'Aktuell',
    'TRWTHEME_SETOPTIONLENGTH_HELP'         => 'Es besteht die Auswahl zwischen 100 (Orig.) und 255 (Empfohlen). Achtung beim zurücksetzen der Einstellung können Informationen verloren gehen oder abgeschnitten werden.',
    'TRWTHEME_SUCCESS_SETFIELDSIZE'         => 'Datenbank Feldgröße wurde angepasst.',
    'TRWTHEME_SUCCESS_IMPORTTHEMEOPTIONS'   => 'Theme wurde importiert.',
    'TRWTHEME_SUCCESS_SETNEWTHEMEOPTION'    => 'Neue Option wurde gesetzt.',
    'TRWTHEME_SUCCESS_TRANSFERTHEMEOPTIONS' => 'Theme wurde transferiert.',
    'TRWTHEME_SUCCESS_DELETEALLTHEMEOPTION' => 'Alle Theme Einstellungen des gewählten Themes wurden gelöscht.',
    'TRWTHEME_SUCCESS_DELETETHEMEOPTION'    => 'Theme-Einstellung wurden gelöscht.',

    'TRWTHEME_ADMINTHEME_NOT_LOADED' => 'Admin-Theme kann nicht geladen werden',
    'TRWTHEME_THEME_ACTIVATE_HELP'   => 'Aktiviert das Admin Theme und leert im Anschluss den Cache des Admin Themes.',
];
