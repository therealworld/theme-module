<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'mxtrwtheme'                   => 'ChildTheme Konfigurator',
    'mxtrwthemeadmin'              => 'Admin-Themes',
    'mxtrwthemeadminmain'          => 'Main',
    'mxtrwthemeadminconfiguration' => 'Configuration',

    'TRWTHEME_OPTIONS'                      => 'ChildTheme Configurator',
    'TRWTHEME_PARENTTHEME'                  => 'Parenttheme',
    'TRWTHEME_ACTIVETHEME'                  => 'active Theme',
    'TRWTHEME_CHILDTHEME_WARNING'           => 'Attention! The current theme is not a Childtheme.',
    'TRWTHEME_TRANSFER'                     => 'Parenttheme-Options',
    'TRWTHEME_TRANSFER_GO'                  => 'assume',
    'TRWTHEME_TRANSFER_HELP'                => 'Transfers all not yet present in the child theme Parent Theme settings in the current Childtheme.',
    'TRWTHEME_EDIT'                         => 'edit',
    'TRWTHEME_GROUP_RENAME'                 => 'Rename Theme-Option-Gruppen-ID',
    'TRWTHEME_GROUP_RENAME_HELP'            => 'Set a new ID for one og the following Theme-Group-IDs.',
    'TRWTHEME_GROUP_NEW'                    => 'New Theme-Option-Gruppen-ID',
    'TRWTHEME_GROUP_ADD'                    => 'Add Group',
    'TRWTHEME_OPTION_EDIT'                  => 'Edit Theme Option',
    'TRWTHEME_OPTION_SELECT_EDIT'           => 'Edit Theme Option of Type "Select"',
    'TRWTHEME_OPTION_SELECT_EDIT_HELP'      => 'Select settings can not be complete in the Theme Options. Therefore there is the possibility to manipulate values. Values = "|" seperated.',
    'TRWTHEME_OPTION_DELETE'                => 'Delete Theme option(s)',
    'TRWTHEME_OPTION_DELETE_ALL'            => 'Delete All Options from All Themes',
    'TRWTHEME_OPTION_DELETE_ALL_HELP'       => '!Should only be used with large clean-up operations!',
    'TRWTHEME_OPTION_DELETE_ALL_FROM_THEME' => 'Delete All Options from Theme',
    'TRWTHEME_OPTION_DELETE_ONCE'           => 'Delete this Theme Option',
    'TRWTHEME_OPTION_GROUP'                 => 'Gruppe',
    'TRWTHEME_OPTION_GROUP_NOTE'            => '!Attention! Possibly Admin language constants must still be created or corrected for the group.',
    'TRWTHEME_OPTION_NAME'                  => 'Name',
    'TRWTHEME_OPTION_NAME_NOTE'             => '!Attention! Possibly Admin language constants must still be created or corrected for the variable.',
    'TRWTHEME_OPTION_TYPE'                  => 'Typ',
    'TRWTHEME_EXPORT'                       => 'Export Theme',
    'TRWTHEME_TARGETTHEME'                  => 'Name of Target Theme',
    'TRWTHEME_TARGETTHEME_HELP'             => 'Enter the name of the theme, for that the theme settings to be imported.',
    'TRWTHEME_IMPORT'                       => 'Import Theme',
    'TRWTHEME_IMPORT_ERROR'                 => 'Attention! Either config Yaml file is not valid or the target theme does not exist.',
    'TRWTHEME_EXPORT_ERROR'                 => 'Attention! There may not be any theme settings in your export theme.',
    'TRWTHEME_SETNEWTHEMEOPTION'            => 'create new Childtheme-Option',
    'TRWTHEME_CONFIG_TYPE_BOOL'             => 'Boolean',
    'TRWTHEME_CONFIG_TYPE_STR'              => 'String',
    'TRWTHEME_CONFIG_TYPE_ARR'              => 'Array',
    'TRWTHEME_CONFIG_TYPE_AARR'             => 'Associative Array',
    'TRWTHEME_CONFIG_TYPE_SELECT'           => 'Select-List',
    'TRWTHEME_EDIT_SELECT_VALUE_HELP'       => 'note Options pipe-seperated. e.g. contact|list|detail',
    'TRWTHEME_SETOPTIONLENGTH'              => 'OXID Optionlength',
    'TRWTHEME_NEW'                          => 'New',
    'TRWTHEME_ACTUAL'                       => 'Actual',
    'TRWTHEME_SETOPTIONLENGTH_HELP'         => 'There is a choice between 100 (Orig.) And 255 (Recommended). Caution When resetting the setting, information may be lost or truncated.',
    'TRWTHEME_SUCCESS_SETFIELDSIZE'         => 'customized Database Fieldsize.',
    'TRWTHEME_SUCCESS_IMPORTTHEMEOPTIONS'   => 'Theme imported.',
    'TRWTHEME_SUCCESS_SETNEWTHEMEOPTION'    => 'New option has been set.',
    'TRWTHEME_SUCCESS_TRANSFERTHEMEOPTIONS' => 'transferred Theme.',
    'TRWTHEME_SUCCESS_DELETEALLTHEMEOPTION' => 'All Theme settings of the selected themes have been deleted.',
    'TRWTHEME_SUCCESS_DELETETHEMEOPTION'    => 'Theme-Option have been deleted.',

    'TRWTHEME_ADMINTHEME_NOT_LOADED' => 'Admin-Theme cannot be loaded',
    'TRWTHEME_THEME_ACTIVATE_HELP'   => 'Activates the Admin-Theme and then clears the cache of the Admin-Theme.',
];
