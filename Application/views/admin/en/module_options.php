<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwthemeswitcher' => 'Developer-Options',

    'SHOP_MODULE_bTRWThemeSwitcherFrontend' => 'activate Theme-Switch in Frontend?',
    'SHOP_MODULE_bTRWThemeOnlyChilds'       => 'Show only Child-Themes in Frontend',
    'SHOP_MODULE_sTRWThemeMainTheme'        => 'Main-Theme',
    'SHOP_MODULE_iTRWThemeResetToMainTheme' => 'After how many seconds of inactivity, you should switch back to the Main-Theme?',
];
