[{assign var="sActiveClass" value=$oViewConf->getTopActiveClassName()}]
<div id="trwthemeswitchbox" class="panel panel-default">
    <h3 class="panel-heading">[{oxmultilang ident="TRWTHEME_SWITCH_HEAD"}]</h3>
    <ul class="panel-body">
        [{foreach from=$oThemeList item="oTheme"}]
            [{if !$oViewConf->getConfigParam('bTRWThemeOnlyChilds') || $oTheme->getParent()}]
                [{assign var="sThemeId" value=$oTheme->getId()}]
                <li>
                    <a href="[{oxgetseourl ident=$oViewConf->getRequestUri()|oxaddparams:"themeid=$sThemeId"}]" title="[{$oTheme->getInfo('title')|htmlentities}]">[{$oTheme->getInfo('id')}]</a>
                </li>
            [{/if}]
        [{/foreach}]
    </ul>
</div>
