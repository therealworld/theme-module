[{if $module_var=='sTRWThemeMainTheme'}]
    <select class="select" name="confselects[[{$module_var}]]" [{$readonly}]>
        [{foreach from=$var_constraints.$module_var item='_field'}]
        <option value="[{$_field|escape}]" [{if ($confselects.$module_var==$_field)}]selected[{/if}]>[{$_field|escape}]</option>
        [{/foreach}]
    </select>
[{else}]
    [{$smarty.block.parent}]
[{/if}]