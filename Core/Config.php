<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ThemeModule\Core;

use OxidEsales\Eshop\Core\Exception\StandardException;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Theme;
use OxidEsales\EshopCommunity\Internal\Framework\Theme\Bridge\AdminThemeBridgeInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use TheRealWorld\ThemeModule\Application\Model\AdminTheme;

/**
 * Main shop configuration class.
 */
class Config extends Config_parent
{
    /** Indicates if themeswitch has been already run. */
    protected bool $_bRunThemeSwitch = false;

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    public function getDir(
        $file,
        $dir,
        $admin,
        $lang = null,
        $shop = null,
        $theme = null,
        $absolute = true,
        $ignoreCust = false
    ) {
        if ($admin) {
            return $this->getAdminThemeManagerDir($file, $dir, $admin, $lang, $shop, $theme, $absolute, $ignoreCust);
        }

        return parent::getDir($file, $dir, $admin, $lang, $shop, $theme, $absolute, $ignoreCust);
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    public function getAdminThemeManagerDir(
        $file,
        $dir,
        $admin,
        $lang = null,
        $shop = null,
        $theme = null,
        $absolute = true,
        $ignoreCust = false
    ) {
        if (is_null($theme)) {
            $theme = $this->getConfigParam('sAdminTheme') ??
                $this->getContainer()->
                    get(AdminThemeBridgeInterface::class)->
                    getActiveTheme();
        }

        if ($dir !== $this->_sTemplateDir) {
            $base = $this->getOutDir($absolute);
            $absBase = $this->getOutDir();
        } else {
            $base = $this->getViewsDir($absolute);
            $absBase = $this->getViewsDir();
        }

        $langAbbr = '-';
        // false means skip language folder check
        if ($lang !== false) {
            $language = Registry::getLang();

            if (is_null($lang)) {
                $lang = $language->getEditLanguage();
            }

            $langAbbr = $language->getLanguageAbbr($lang);
        }

        if (is_null($shop)) {
            $shop = $this->getShopId();
        }

        // Load from
        $path = $theme .
            DIRECTORY_SEPARATOR . $shop .
            DIRECTORY_SEPARATOR . $langAbbr .
            DIRECTORY_SEPARATOR . $dir .
            DIRECTORY_SEPARATOR . $file;
        $cacheKey = $path . "_{$ignoreCust}{$absolute}";

        if (($return = Registry::getUtils()->fromStaticCache($cacheKey)) !== null) {
            return $return;
        }

        $return = $this->getEditionTemplate($theme . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . $file);

        // Check for custom template
        $customTheme = $this->getConfigParam('sAdminCustomTheme');
        if (!$return && !$ignoreCust && $customTheme && $customTheme !== $theme) {
            $return = $this->getAdminThemeManagerDir($file, $dir, $admin, $lang, $shop, $customTheme, $absolute);
        }

        // test lang level ..
        if (!$return && is_readable($absBase . $path)) {
            $return = $base . $path;
        }

        // test shop level ..
        if (!$return) {
            $return = $this->getShopLevelDir(
                $base,
                $absBase,
                $file,
                $dir,
                $admin,
                $lang,
                $shop,
                $theme,
                $absolute,
                $ignoreCust
            );
        }
        // </editor-fold>

        // test theme language level ..
        $path = $theme . DIRECTORY_SEPARATOR . $langAbbr . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . $file;
        if (!$return && $lang !== false && is_readable($absBase . $path)) {
            $return = $base . $path;
        }

        // test theme level ..
        $path = $theme . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . $file;
        if (!$return && is_readable($absBase . $path)) {
            $return = $base . $path;
        }

        // test out language level ..
        $path = $langAbbr . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . $file;
        if (!$return && $lang !== false && is_readable($absBase . $path)) {
            $return = $base . $path;
        }

        // test out level ..
        $path = $dir . DIRECTORY_SEPARATOR . $file;
        if (!$return && is_readable($absBase . $path)) {
            $return = $base . $path;
        }

        // to cache
        Registry::getUtils()->toStaticCache($cacheKey, $return);

        return $return;
    }

    public function getAdminThemeManagerSelectedTheme(): string
    {
        /** @var AdminTheme $activeTheme */
        $activeTheme = oxNew(AdminTheme::class);

        return $activeTheme->getActiveThemeId();
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected function _loadVarsFromDb($sShopID, $aOnlyVars = null, $sModule = '')
    {
        $bResult = parent::_loadVarsFromDb($sShopID, $aOnlyVars, $sModule);

        if (!$this->_bRunThemeSwitch) {
            $this->_bRunThemeSwitch = true;

            // preload the themeswitch-option, because in the first run, this option issnt load
            parent::_loadVarsFromDb($sShopID, 'bTRWThemeSwitcherFrontend', 'module:trwtheme');

            if ($this->getConfigParam('bTRWThemeSwitcherFrontend')) {
                $bTRWThemeOnlyChilds = $this->getConfigParam('bTRWThemeOnlyChilds');
                $sTRWThemeMainThemeId = $this->getConfigParam('sTRWThemeMainTheme');

                $oTheme = oxNew(Theme::class);
                if ($sThemeId = Registry::getRequest()->getRequestParameter('themeid')) {
                    $bThemeLoadSuccess = $oTheme->load($sThemeId);
                    if (
                        $bThemeLoadSuccess === false
                        || ($bTRWThemeOnlyChilds && !$oTheme->getParent())
                    ) {
                        $oTheme->load($sTRWThemeMainThemeId);
                    }

                    try {
                        $oTheme->activate();
                        Registry::getUtils()->oxResetFileCache();
                    } catch (StandardException $oEx) {
                        Registry::getUtilsView()->addErrorToDisplay($oEx);
                        $oEx->debugOut();
                    }
                }
            }
        }

        return $bResult;
    }
}
