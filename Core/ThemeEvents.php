<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ThemeModule\Core;

use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use TheRealWorld\ThemeModule\Application\Model\AdminTheme;
use TheRealWorld\ToolsPlugin\Core\ToolsConfig;

class ThemeEvents
{
    /**
     * OXID-Core.
     *
     * @throws ContainerExceptionInterface
     * @throws DatabaseConnectionException
     * @throws DatabaseErrorException
     * @throws NotFoundExceptionInterface
     */
    public static function onActivate()
    {
        // "first"-creation if not exists
        foreach (ToolsConfig::getShopIds() as $iShopId) {
            $aAdminThemeConfig = ToolsConfig::getShopConfigValues(
                ['sAdminTheme'],
                true,
                $iShopId
            );

            if (count($aAdminThemeConfig) === 0) {
                $adminTheme = oxNew(AdminTheme::class);
                $adminTheme->loadOxAdmin();
                $adminTheme->saveConfiguration();
            }
        }

        return true;
    }

    /**
     * OXID-Core.
     */
    public static function onDeactivate()
    {
        return true;
    }
}
