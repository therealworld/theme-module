<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ThemeModule\Core;

use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\StandardException;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Theme;
use TheRealWorld\ToolsPlugin\Core\ToolsDB;

/**
 * class for output processing.
 */
class Output extends Output_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws DatabaseConnectionException
     */
    public function process($sValue, $sClassName)
    {
        $oConfig = Registry::getConfig();
        $oTheme = oxNew(Theme::class);

        $sValue = parent::process($sValue, $sClassName);

        // check id theme reset is neccessary
        $sTRWThemeMainThemeId = $oConfig->getConfigParam('sTRWThemeMainTheme');
        if (
            ($oTheme->getActiveThemeId() !== $sTRWThemeMainThemeId)
            && ($iTRWThemeResetToMainTheme = $oConfig->getConfigParam('iTRWThemeResetToMainTheme'))
        ) {
            $sActualThemeTime = ToolsDB::getAnyId(
                'oxconfig',
                ['oxvarname' => 'sTheme'],
                'oxtimestamp'
            );
            $iActualThemeTime = strtotime($sActualThemeTime);
            if (($iActualThemeTime + $iTRWThemeResetToMainTheme) < time()) {
                if ($oTheme->load($sTRWThemeMainThemeId)) {
                    try {
                        $oTheme->activate();
                        Registry::getUtils()->oxResetFileCache();
                    } catch (StandardException $oEx) {
                        Registry::getUtilsView()->addErrorToDisplay($oEx);
                        $oEx->debugOut();
                    }
                }
            } else {
                $sSql = ToolsDB::createSingleTableExecuteSql(
                    'oxconfig',
                    [
                        'oxtimestamp' => 'now()',
                    ],
                    'update',
                    [[
                        'operator'  => 'and',
                        'condition' => '=',
                        'field'     => 'oxvarname',
                        'value'     => 'sTheme',
                        'noquote'   => false,
                    ]]
                );
                ToolsDB::execute($sSql);
            }
        }

        if (
            !isAdmin()
            && $oConfig->getConfigParam('bTRWThemeSwitcherFrontend')
            && ($iBodyEndPos = strpos($sValue, '</body>')) !== false
        ) {
            $sValue = substr_replace(
                $sValue,
                $this->_renderThemeSwitchWidget(),
                $iBodyEndPos,
                0
            );
        }

        return $sValue;
    }

    /**
     * render the ThemeSwitch Widget.
     *
     * @return string
     */
    public function _renderThemeSwitchWidget()
    {
        $oTheme = oxNew(Theme::class);
        $oSmarty = Registry::getUtilsView()->getSmarty();
        $oSmarty->assign('oThemeList', $oTheme->getList());

        return $oSmarty->fetch('themeswitchwidget.tpl');
    }
}
