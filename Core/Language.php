<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\ThemeModule\Core;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\EshopCommunity\Internal\Framework\Theme\Bridge\AdminThemeBridgeInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

/**
 * Class Language.
 */
class Language extends Language_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    public function getAdminTplLanguageArray()
    {
        if ($this->_aAdminTplLanguageArray === null) {
            $oConfig = Registry::getConfig();
            $sTheme = $oConfig->getConfigParam('sAdminTheme') ??
                $this->getContainer()->
                    get(AdminThemeBridgeInterface::class)->
                    getActiveTheme();

            $aLangArray = $this->getLanguageArray();
            $this->_aAdminTplLanguageArray = [];

            $sSourceDir = $oConfig->getAppDir() . 'views' .
                DIRECTORY_SEPARATOR . $sTheme .
                DIRECTORY_SEPARATOR;
            foreach ($aLangArray as $iLangKey => $oLang) {
                $sFilePath = "{$sSourceDir}{$oLang->abbr}/lang.php";
                if (file_exists($sFilePath) && is_readable($sFilePath)) {
                    $this->_aAdminTplLanguageArray[$iLangKey] = $oLang;
                }
            }

            $sCustomTheme = $oConfig->getConfigParam('sAdminCustomTheme');
            if ($sCustomTheme) {
                $sSourceDir = $oConfig->getAppDir() . 'views' .
                    DIRECTORY_SEPARATOR . $sCustomTheme .
                    DIRECTORY_SEPARATOR;
                foreach ($aLangArray as $iLangKey => $oLang) {
                    $sFilePath = $sSourceDir . $oLang->abbr . DIRECTORY_SEPARATOR . 'lang.php';
                    if (file_exists($sFilePath) && is_readable($sFilePath)) {
                        $this->_aAdminTplLanguageArray[$iLangKey] = $oLang;
                    }
                }
            }
        }

        // moving pointer to beginning
        reset($this->_aAdminTplLanguageArray);

        return $this->_aAdminTplLanguageArray;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    protected function _getAdminLangFilesPathArray($iLang)
    {
        $oConfig = Registry::getConfig();
        $aLangFiles = [];
        $sTheme = $oConfig->getConfigParam('sAdminTheme') ??
            $this->getContainer()->
            get(AdminThemeBridgeInterface::class)->
            getActiveTheme();

        $sAppDir = $oConfig->getAppDir();
        $sLang = Registry::getLang()->getLanguageAbbr($iLang);

        $aModulePaths = [];
        $aModulePaths = array_merge($aModulePaths, $this->_getActiveModuleInfo());
        $aModulePaths = array_merge($aModulePaths, $this->_getDisabledModuleInfo());

        // admin lang files
        $sAdminPath = $sAppDir . 'views' .
            DIRECTORY_SEPARATOR . $sTheme .
            DIRECTORY_SEPARATOR . $sLang;
        $aLangFiles[] = $sAdminPath .
            DIRECTORY_SEPARATOR . 'lang.php';
        $aLangFiles[] = $sAppDir . 'translations' .
            DIRECTORY_SEPARATOR . $sLang .
            DIRECTORY_SEPARATOR . 'translit_lang.php';
        $aLangFiles = $this->_appendLangFile($aLangFiles, $sAdminPath);

        $aLangFiles = array_merge($aLangFiles, $this->getAdminCustomThemeLanguageFiles($iLang));

        // themes options lang files
        $sThemePath = $sAppDir . 'views' .
            DIRECTORY_SEPARATOR . '*' .
            DIRECTORY_SEPARATOR . $sLang;
        $aLangFiles = $this->_appendLangFile($aLangFiles, $sThemePath, 'options');

        // module language files
        $aLangFiles = $this->_appendModuleLangFiles($aLangFiles, $aModulePaths, $sLang, true);

        // custom language files
        $aLangFiles = $this->_appendCustomLangFiles($aLangFiles, $sLang, true);

        return count($aLangFiles) ? $aLangFiles : false;
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    protected function _appendCustomLangFiles($aLangFiles, $sLang, $blForAdmin = false)
    {
        if ($blForAdmin) {
            /** @var Config $oConfig */
            $oConfig = Registry::getConfig();
            $sAppDir = $oConfig->getAppDir();
            $sTheme = $oConfig->getConfigParam('sAdminTheme') ??
                $this->getContainer()->
                get(AdminThemeBridgeInterface::class)->
                getActiveTheme();

            $sCustomTheme = $oConfig->getConfigParam('sAdminCustomTheme');

            if ($sTheme) {
                $aLangFiles[] = $sAppDir . 'views' .
                    DIRECTORY_SEPARATOR . $sTheme .
                    DIRECTORY_SEPARATOR . $sLang .
                    DIRECTORY_SEPARATOR . 'cust_lang.php';
            }

            if ($sCustomTheme) {
                $aLangFiles[] = $sAppDir . 'views' .
                    DIRECTORY_SEPARATOR . $sCustomTheme .
                    DIRECTORY_SEPARATOR . $sLang .
                    DIRECTORY_SEPARATOR . 'cust_lang.php';
            }

            return $aLangFiles;
        }

        return parent::_appendCustomLangFiles($aLangFiles, $sLang, $blForAdmin);
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected function _getLanguageMap($iLang, $blAdmin = null)
    {
        $blAdmin ??= $this->isAdmin();
        $sKey = $iLang . (int) $blAdmin;
        if (!isset($this->_aLangMap[$sKey])) {
            $this->_aLangMap[$sKey] = [];

            /** @var Config $oConfig */
            $oConfig = Registry::getConfig();
            $adminTheme = $oConfig->getAdminThemeManagerSelectedTheme();

            $sMapFile = '';
            $sParentMapFile = $oConfig->getAppDir() . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR
                . ($blAdmin ? $adminTheme : $oConfig->getConfigParam('sTheme'))
                . DIRECTORY_SEPARATOR
                . Registry::getLang()->getLanguageAbbr($iLang) . DIRECTORY_SEPARATOR . 'map.php';
            $sCustomThemeMapFile = $oConfig->getAppDir() . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR
                . ($blAdmin ? $adminTheme : $oConfig->getConfigParam('sCustomTheme'))
                . DIRECTORY_SEPARATOR
                . Registry::getLang()->getLanguageAbbr($iLang) . DIRECTORY_SEPARATOR . 'map.php';

            if (file_exists($sCustomThemeMapFile) && is_readable($sCustomThemeMapFile)) {
                $sMapFile = $sCustomThemeMapFile;
            } elseif (file_exists($sParentMapFile) && is_readable($sParentMapFile)) {
                $sMapFile = $sParentMapFile;
            }

            if ($sMapFile) {
                $aMap = [];

                include $sMapFile;
                $this->_aLangMap[$sKey] = $aMap;
            }
        }

        return $this->_aLangMap[$sKey];
    }

    /**
     * OXID-Core.
     * {@inheritDoc}
     */
    protected function getAdminCustomThemeLanguageFiles($language)
    {
        $oConfig = Registry::getConfig();
        $sCustomTheme = $oConfig->getConfigParam('sAdminCustomTheme');
        $sAppDir = $oConfig->getAppDir();
        $sLang = Registry::getLang()->getLanguageAbbr($language);
        $aLangFiles = [];

        if ($sCustomTheme) {
            $sCustPath = $sAppDir . 'views' . DIRECTORY_SEPARATOR . $sCustomTheme . DIRECTORY_SEPARATOR . $sLang;
            $aLangFiles[] = $sCustPath . DIRECTORY_SEPARATOR . 'lang.php';
            $aLangFiles = $this->_appendLangFile($aLangFiles, $sCustPath);
        }

        return $aLangFiles;
    }
}
