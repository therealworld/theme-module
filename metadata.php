<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// Metadata version.

use OxidEsales\Eshop\Application\Controller\Admin\ModuleConfiguration as OxModuleConfiguration;
use OxidEsales\Eshop\Application\Controller\Admin\NavigationTree as OxNavigationTree;
use OxidEsales\Eshop\Core\Config as OxConfig;
use OxidEsales\Eshop\Core\Language as OxLanguage;
use OxidEsales\Eshop\Core\Output as OxOutput;
use TheRealWorld\ThemeModule\Application\Controller\Admin\AdminThemeConfiguration;
use TheRealWorld\ThemeModule\Application\Controller\Admin\AdminThemeController;
use TheRealWorld\ThemeModule\Application\Controller\Admin\AdminThemeList;
use TheRealWorld\ThemeModule\Application\Controller\Admin\AdminThemeMain;
use TheRealWorld\ThemeModule\Application\Controller\Admin\ModuleConfiguration;
use TheRealWorld\ThemeModule\Application\Controller\Admin\NavigationTree;
use TheRealWorld\ThemeModule\Application\Controller\Admin\ThemeController;
use TheRealWorld\ThemeModule\Core\Config;
use TheRealWorld\ThemeModule\Core\Language;
use TheRealWorld\ThemeModule\Core\Output;
use TheRealWorld\ToolsPlugin\Core\ToolsModuleVersion;

$sMetadataVersion = '2.1';

/**
 * Module information.
 */
$aModule = [
    'id'    => 'trwtheme',
    'title' => [
        'de' => 'the-real-world - Admin & ChildTheme Konfigurator',
        'en' => 'the-real-world - Admin & ChildTheme Configutator',
    ],
    'description' => [
        'de' => 'Hilft OXID Childthemes korrekt einzurichten / Admin-Themes / Theme-Switcher Frontend',
        'en' => 'Help to configure OXID childthemes correctly / Admin-Themes / Theme-Switcher Frontend',
    ],
    'thumbnail' => 'picture.png',
    'version'   => ToolsModuleVersion::getModuleVersion('trwtheme'),
    'author'    => 'Mario Lorenz',
    'url'       => 'https://www.the-real-world.de',
    'email'     => 'mario_lorenz@the-real-world.de',
    'events'    => [
        'onActivate'   => '\TheRealWorld\ThemeModule\Core\ThemeEvents::onActivate',
        'onDeactivate' => '\TheRealWorld\ThemeModule\Core\ThemeEvents::onDeactivate',
    ],
    'extend' => [
        // Core
        OxConfig::class   => Config::class,
        OxLanguage::class => Language::class,
        OxOutput::class   => Output::class,
        // Controller
        OxModuleConfiguration::class => ModuleConfiguration::class,
        OxNavigationTree::class      => NavigationTree::class,
    ],
    'controllers' => [
        'AdminThemeConfiguration' => AdminThemeConfiguration::class,
        'AdminThemeController'    => AdminThemeController::class,
        'AdminThemeList'          => AdminThemeList::class,
        'AdminThemeMain'          => AdminThemeMain::class,
        'ThemeController'         => ThemeController::class,
    ],
    'templates' => [
        'admintheme.tpl'        => 'trw/trwtheme/Application/views/admin/tpl/admintheme.tpl',
        'adminthemeconfig.tpl'  => 'trw/trwtheme/Application/views/admin/tpl/adminthemeconfig.tpl',
        'adminthememain.tpl'    => 'trw/trwtheme/Application/views/admin/tpl/adminthememain.tpl',
        'adminthemelist.tpl'    => 'trw/trwtheme/Application/views/admin/tpl/adminthemelist.tpl',
        'themeconfig.tpl'       => 'trw/trwtheme/Application/views/admin/tpl/themeconfig.tpl',
        'themeswitchwidget.tpl' => 'trw/trwtheme/Application/views/theme/tpl/widget/footer/themeswitchwidget.tpl',
    ],
    'blocks' => [
        [
            'template' => 'layout/base.tpl',
            'block'    => 'head_css',
            'file'     => 'Application/views/blocks/layout/head_css.tpl',
        ],
        [
            'template' => 'module_config.tpl',
            'block'    => 'admin_module_config_var_type_select',
            'file'     => 'Application/views/blocks/admin_module_config_var_type_select.tpl',
        ],
    ],
    'settings' => [
        [
            'group' => 'trwthemeswitcher',
            'name'  => 'bTRWThemeSwitcherFrontend',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwthemeswitcher',
            'name'  => 'bTRWThemeOnlyChilds',
            'type'  => 'bool',
            'value' => false,
        ],
        [
            'group' => 'trwthemeswitcher',
            'name'  => 'sTRWThemeMainTheme',
            'type'  => 'select',
            'value' => '',
        ],
        [
            'group' => 'trwthemeswitcher',
            'name'  => 'iTRWThemeResetToMainTheme',
            'type'  => 'num',
            'value' => 3600,
        ],
    ],
];
